# Kubernetes | Services Nodeport & namespace : créer un environnement isolé et exposer des pods vers l'extérieur

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce laboratoire vise à présenter les étapes de mise en place d'un namespace et d'un service de type NodePort sur Kubernetes.

## Objectifs

Dans ce lab, nous allons créer un environnement isolé sur Kubernetes et déployer une application accessible depuis l'extérieur du cluster. Voici les étapes à suivre pour ce processus :

1. **Création d'un Namespace** : Rédigez un manifeste `namespace.yml` pour créer un namespace appelé `production`.

2. **Déploiement d'un Pod** : Créez un manifeste `pod-red.yml` pour déployer un pod utilisant l'image `mmumshad/simple-webapp-color`, avec la couleur rouge spécifiée et un label `app:web`.

3. **Exposition du Pod via NodePort** : Élaborez un manifeste `service-nodeport-web.yml` qui expose le pod sur le réseau extérieur via un service de type NodePort. Configurez ce service pour utiliser le port 30008 comme NodePort et rediriger les requêtes vers le port 8080 des pods.

4. **Lancement et Vérification** : Déployez le service et utilisez `kubectl describe` pour vérifier que le service a correctement identifié les deux pods sous le label `app:web`.

5. **Test d'Accessibilité** : Testez l'accès à l'application en ouvrant le port 30008 du nœud pour s'assurer que l'application est accessible.


## 1. Définition des conceptes

### 1.1. C'est quoi un namespace ?
Un namespace dans Kubernetes est une sorte de partition virtuelle au sein d'un cluster, qui permet d'isoler les groupes de ressources. Chaque namespace peut contenir des ressources (comme des pods, services, volumes, etc.) indépendamment des autres namespaces. Utiliser des namespaces permet de gérer efficacement les droits d'accès, de simplifier l'allocation des ressources, de segmenter le cluster pour différents utilisateurs ou projets, et d'organiser les ressources de manière plus structurée, facilitant ainsi la gestion et la maintenance du cluster.


### 1.2. Concepte de Nodeport
Le service de type NodePort dans Kubernetes permet d'exposer des services sur des ports fixes des nœuds du cluster. Chaque nœud redirige automatiquement ce port fixe (NodePort) vers le port du service. Ce mécanisme rend le service accessible de l'extérieur du cluster en utilisant <IP du Nœud>:<NodePort>. NodePort est généralement utilisé pour des applications de test ou des environnements de développement, car il offre un moyen simple et direct d'accéder aux services à travers les adresses IP des nœuds.


## 2. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)



## 2. Création d'un cluster

Consultez le document [Istaller kubeadm](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)


## 3. Configuration initiale

### 3.1. Création du fichier namespace.yml pour le namespace production.
````bash
nano namespace.yml
````

````yaml
apiVersion: v1
kind: Namespace
metadata:
  name: production
````

### 3.2. Création du namespace.

````bash
kubectl apply -f namespace.yml
````

>![alt text](img/image.png)
*Création du namespace effectuée*

````bash 
kubectl get namespace
````
>![alt text](img/image-1.png)
*Name space "production" crée*

## 4. Déploiement des Pods

### 4.1. Description du fichier pod-red.yml
````bash
nano pod-red.yml
````

````yaml
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp-color-red
  labels:
    app: web
spec:
  containers:
    - name: web
      image: mmumshad/simple-webapp-color
      ports:
        - containerPort: 8080
      env:
        - name: APP_COLOR
          value: red #définition de la couleur
````

### 4.2. Procédure pour déployer le pod dans le namespace production.

````bash
kubectl apply -f pod-red.yml -n production
````

Vérification de la création du pod dans le namespace définit 

````bash
kubectl get pod --namespace production

ou 

kubectl get pod -n production
````

>![alt text](img/image-3.png)


### 4.3. Description du fichier pod-blue.yml
````bash
nano pod-blue.yml
````

````yaml
apiVersion: v1
kind: Pod
metadata:
  name: simple-webapp-color-blue
  labels:
    app: web
spec:
  containers:
    - name: web
      image: mmumshad/simple-webapp-color
      ports:
        - containerPort: 8080
      env:
        - name: APP_COLOR
          value: blue #définition de la couleur
````

### 4.4. Création du pod blue
````bash
kubectl apply -f pod-blue.yml -n production
````

>![alt text](img/image-4.png)
*Pod blue en cours d'exécution*


## 5. Création du Service NodePort

### 5.1. Description du fichier service-nodeport-web.yml
````bash
nano service-nodeport-web.yml
````

````yaml
apiVersion: v1
kind: Service
metadata:
  name: service-nodeport-web
spec:
  type: NodePort
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 8080
    nodePort: 30000
````

Dans ce manifest, nous définissons un service de type NodePort ici désigné par **service-nodeport-web**. Il est configuré pour router le trafic TCP sur le port 8080 vers le targetPort 8080 des pods étiquetés avec app: web. De plus, ce service est exposé sur chaque nœud du cluster via le nodePort 30000. Cette configuration permet au trafic externe d'accéder aux pods spécifiés via l'adresse IP de n'importe quel nœud du cluster, suivi du port 30000.

### 5.2. Création du service Nodeport et exposition des pods

````bash
kubectl apply -f service-nodeport-web.yml -n production
````
>![alt text](img/image-5.png)
*Service est bien crée*

On peut vérifier les détails de notre service pour obtenir les IPs sur lesquelles il pointe

````bash
kubectl describe service -n production
````

>![alt text](img/image-6.png)


## 6. Vérification et Tests

En entrant l'adresse IP de l'un des node du cluster, dans notre cas, celui du **"node control"** dans un navigateur suivi du port 3000, l'on obtiens une page du navigateur qui s'alterne.

>![alt text](img/image-7.png)
*webapp bleu*

>![alt text](img/image-8.png)
*webapp red*

on peut constater que l'exposition des deux applications (red & blue) vers l'extérieur et le loadbalancing marchent bien, car il permet d'alterner entre les deux pages.



## Documentation 

###  [Ingress-Nginx Controller](https://kubernetes.github.io/ingress-nginx/deploy/#quick-start)

### [Ingress-Nginx Controller pour Minikube](https://kubernetes.github.io/ingress-nginx/deploy/#provider-specific-steps)

### [Installer kubeadm sur ubuntu](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

### [Creating a Single Control-Plane Cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
